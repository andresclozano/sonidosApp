import { Component } from '@angular/core';
import { ANIMALES } from '../../data/data.animales'
import { Animal } from '../../interfaces/animal.interface';
import { reorderArray } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  animales:Animal[];
  audio = new Audio();
  audioTiempo: any;
  ordenando:boolean = false;

  constructor() {
    this.animales = ANIMALES.slice(0);
  }

  reproducir(animal:Animal){
    this.pausar_audio(animal);
    if(animal.reproduciendo){
      animal.reproduciendo = false;
      return;
    }

    this.audio.src = animal.audio;
    this.audio.load();
    this.audio.play();
    animal.reproduciendo = true;

    this.audioTiempo = setTimeout(()=> animal.reproduciendo = false, (1000 * animal.duracion));
  }

  eliminar(index:number){
    this.animales.splice(index, 1);
  }

  refrescar(event:any){
    setTimeout(()=>{
      this.animales = ANIMALES.slice(0);
      event.complete();
    }, 2000)
  }

  reordenar(indices:any){
    this.animales = reorderArray(this.animales, indices);
  }

  private pausar_audio(animal:Animal){
    clearTimeout(this.audioTiempo);
    this.audio.pause();
    this.audio.currentTime = 0;
    for(let a of this.animales){
      if(a.nombre != animal.nombre){
        a.reproduciendo = false;
      }
    }

  }


}
